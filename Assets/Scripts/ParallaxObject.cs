﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxObject : MonoBehaviour
{
    [Range(0, 1)]
    public float parallaxCoef = 0.5f;

    private Vector3 _initialScale = Vector3.zero;
    private Vector3 _lastScale = Vector3.zero;
    private Vector2 _offset = Vector2.zero;

    IEnumerator _scaleObject;

    private void OnEnable()
    {
        CameraMovement.OnCameraMoved += OnCameraMoved;
        CameraMovement.OnCameraZoomed += OnCameraZoomed;
    }

    private void Start()
    {
        _initialScale = _lastScale = transform.lossyScale;
        _offset = (transform.position - Camera.main.transform.position);
    }

    private void OnDisable()
    {
        CameraMovement.OnCameraMoved -= OnCameraMoved;
        CameraMovement.OnCameraZoomed -= OnCameraZoomed;
    }

    private void OnCameraMoved(Vector2 movement)
    {
        transform.Translate(movement * parallaxCoef);
        _offset = (transform.position - Camera.main.transform.position);
        _lastScale = transform.localScale;
    }

    private void OnCameraZoomed(float zoomValue)
    {
        if (_scaleObject != null)
            StopCoroutine(_scaleObject);

        _scaleObject = Scale(_initialScale * (1 + zoomValue * parallaxCoef));
        StartCoroutine(_scaleObject);
    }

    IEnumerator Scale(Vector3 targetScale)
    {
        while (transform.localScale != targetScale)
        {
            transform.localScale = Vector3.MoveTowards(transform.localScale, targetScale, Time.deltaTime);
            transform.position = _offset * transform.localScale.x / _lastScale.x + (Vector2)Camera.main.transform.position;

            yield return null;
        }

        transform.localScale = targetScale;
        _scaleObject = null;
    }
}
