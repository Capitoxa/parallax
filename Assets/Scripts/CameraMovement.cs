﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    public static event System.Action<Vector2> OnCameraMoved;
    public static event System.Action<float> OnCameraZoomed;

    public float cameraMovementSpeed = 1f;
    public float zoomSpeed = 1f;

    private Camera _camera;

    private float _movementX = 0;
    public float MovementX
    {
        get { return _movementX; }
        set
        {
            if (_movementX != value)
            {
                _movementX = value;

                if (OnCameraMoved != null)
                    OnCameraMoved(-Vector2.right * _movementX);
            }
        }
    }
    
    private float _movementY = 0;
    public float MovementY
    {
        get { return _movementY; }
        set
        {
            if (_movementY != value)
            {
                _movementY = value;

                if (OnCameraMoved != null)
                    OnCameraMoved(-Vector2.up * _movementY);
            }
        }
    }

    private float _zoom = 1f;
    public float Zoom
    {
        get { return _zoom; }
        set
        {
            if (_zoom != value)
            {
                _zoom = value;                
                _zoom = Mathf.Clamp(_zoom, 0.1f, Mathf.Infinity);

                if (OnCameraZoomed != null)
                    OnCameraZoomed(_zoom - 1);
            }
        }
    }

    private void Start()
    {
        _camera = Camera.main;
    }

    private void Update()
    {
        MovementX = Input.GetAxis("Horizontal") * cameraMovementSpeed * Time.deltaTime;
        MovementY = Input.GetAxis("Vertical") * cameraMovementSpeed * Time.deltaTime;

        if (Input.GetAxis("Mouse ScrollWheel") > 0)
            Zoom += zoomSpeed * Time.deltaTime;
        else if (Input.GetAxis("Mouse ScrollWheel") < 0)
            Zoom -= zoomSpeed * Time.deltaTime;
    }
}

